const express = require('express')
const WebSocket = require('ws')
const uuid = require('node-uuid')

const PORT = process.env.PORT || 5000
const app = express()

// Answer API requests
app.get('/api', function (req, res) {
  res.set('Content-Type', 'application/json')
  res.send('{"message":"Hello from the custom server!"}')
})

// Start HTTP server
const server = app.listen(PORT, function () {
  console.log(`Listening on port ${PORT}`)
})

// Create ws server
const wss = new WebSocket.Server({ server })

// Globals for ws
const clients = []
let clientIndex = 1
const initialChannels = [
  'general',
  'random'
]

// Global constants
// Message send types
const SEND_LEAVE = 'leave'
const SEND_MSG = 'message'
const SEND_NOTIFY = 'notify'
// Message receive types
const RECEIVE_NICK = '/nick'
const RECEIVE_JOIN = '/join'
const RECEIVE_CHAN = '/chan'
// Socket event type
const EVENT_CLOSE = 'close'
const EVENT_CONN = 'connection'
const EVENT_MSG = 'message'

// WS logic
wss.on(EVENT_CONN, ws => {
  // Create new client
  const client = {
    activeChannel: initialChannels[0],
    channels: initialChannels,
    senderId: uuid.v4(),
    senderNickname: `AnonUser${clientIndex}`,
    ws
  }
  clients.push(client)
  clientIndex += 1
  wsSend(SEND_NOTIFY, `${client.senderNickname} has connected`)

  // Handle message
  ws.on(EVENT_MSG, messageBody => {
    // Ignore empty message
    if (messageBody.length === 0) { return }
    const messageArray = messageBody.trim().split(' ')
    const command = messageArray[0]

    // Nick update
    if (command === RECEIVE_NICK) {
      const nicknameArray = messageBody.split(' ')
      if (nicknameArray.length >= 2) {
        const oldNickname = client.senderNickname
        client.senderNickname = nicknameArray[1]
        wsSend(SEND_NOTIFY, `${oldNickname} changed to ${client.senderNickname}`)
      }

    // Join channel
    } else if (command === RECEIVE_JOIN) {
      const oldChannel = client.activeChannel
      const channelArray = messageBody.split(' ')
      const validChannel = (
        (channelArray.length >= 2) &&
        (client.channels.indexOf(channelArray[1]) !== -1) &&
        (channelArray[1] !== oldChannel)
      )
      if (validChannel) {
        wsSend(SEND_LEAVE, `${client.senderNickname} left channel ${oldChannel}`)
        client.activeChannel = channelArray[1]
        wsSend(SEND_NOTIFY, `${client.senderNickname} joined channel ${client.activeChannel}`)
      }

    // Create channel
    } else if (command === RECEIVE_CHAN) {
      const channelArray = messageBody.split(' ')
      const validChannel = (
        (channelArray.length >= 2) &&
        (client.channels.indexOf(channelArray[1]) === -1)
      )
      if (validChannel) {
        client.channels.push(channelArray[1])
        wsSend(SEND_NOTIFY, `${client.senderNickname} created channel ${channelArray[1]}`)
      }

    // Regular message
    } else {
      wsSend(SEND_MSG, messageBody)
    }
  })

  // Handle connection close
  ws.on(EVENT_CLOSE, () => closeSocket())
  process.on('SIGINT', () => {
    console.log('Shutting down')
    closeSocket('Server has disconnected')
    process.exit()
  })

  // Send message callback
  // * Sender is current user (via closure)
  // * Recipients are members of current user's channel
  function wsSend (type, messageBody) {
    const { activeChannel, channels, senderId, senderNickname } = client
    // Build activeChannel's members list
    const members = clients
      .filter(c => (
        (c.activeChannel === activeChannel) &&
        (c.ws.readyState === WebSocket.OPEN)
      ))
    let memberNicks
    let message

    // Get nicknames from members list
    if (type === SEND_LEAVE) {
      // Exclude current member from member list
      memberNicks = members
        .filter(m => m.senderNickname !== senderNickname)
        .map(m => m.senderNickname)
    } else {
      // Include all members
      memberNicks = members
        .map(m => m.senderNickname)
    }

    // Send message
    members
      .forEach(m => {
        // Build message payload
        message = JSON.stringify({
          activeChannel,
          channels,
          members: memberNicks,
          messageBody,
          recipientNickname: m.senderNickname,
          senderId,
          senderNickname,
          type
        })
        m.ws.send(message)
      })
  }

  // Close socket connection callback
  function closeSocket (customMessageBody) {
    const { senderId, senderNickname, activeChannel, channels } = client
    const type = SEND_NOTIFY
    const i = clients.findIndex(c => c.senderId === client.senderId)
    let message
    const messageBody = customMessageBody || `${client.senderNickname} has disconnected`

    // Ignore callback if client not found
    if (i === -1) {
      return
    }

    // Build member list after excluding current client
    const members = clients
      .filter(c => (
        (c.senderId !== senderId) &&
        (c.activeChannel === activeChannel) &&
        (c.ws.readyState === WebSocket.OPEN)
      ))
    const memberNicks = members
      .map(m => m.senderNickname)

    // send message
    members
      .forEach(m => {
        // build message payload
        message = JSON.stringify({
          activeChannel,
          channels,
          members: memberNicks,
          messageBody,
          recipientNickname: m.senderNickname,
          senderId,
          senderNickname,
          type
        })
        m.ws.send(message)
      })

    // Update clients list
    clients.splice(i, 1)
  }
})
