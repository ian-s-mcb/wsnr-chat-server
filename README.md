# wsnr-chat-server

The backend of a barebones, IRC-like chat app.

The backend uses [Node][node], [Express][express], and the WebSockets
library [ws][ws]. The app is hosted on [Heroku][heroku]. The Heroku app
provisioning was based on [these][heroku-node-and-react]
[two][heroku-node-and-ws] examples from the Heroku docs.

This project connects to the frontend described [here][frontend].

### [Demo][demo]

### Screencast
![Video screencast of the wsnr-chat app in action][screencast]

[node]: https://nodejs.org/
[express]: https://github.com/expressjs/express
[ws]: https://github.com/websockets/ws
[heroku]: https://www.heroku.com/
[heroku-node-and-react]: https://github.com/mars/heroku-cra-node
[heroku-node-and-ws]: https://github.com/heroku-examples/node-ws-test
[frontend]: https://gitlab.com/ian-s-mcb/wsnr-chat-client
[demo]: https://wsnr-chat-client.herokuapp.com/
[screencast]: https://i.imgur.com/PKg2Tv0.mp4
